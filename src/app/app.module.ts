import { HttpModule, JsonpModule } from '@angular/http';
import { Interceptor } from './../auth/interceptor';
import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { DBMeter } from '@ionic-native/db-meter';
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ColorsambientePage } from '../pages/colorsambiente/colorsambiente';
import { Network } from '@ionic-native/network';
import { Dialogs } from '@ionic-native/dialogs';
import { DatePipe } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    ColorsambientePage
  ],
  imports: [
    BrowserModule,
    HttpModule,
    JsonpModule,
    HttpClientModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ColorsambientePage
  ],
  providers: [
    StatusBar,
    DBMeter,
    SplashScreen,
    Interceptor,
    Network,
    DatePipe,
    Dialogs,
    { provide: ErrorHandler, useClass: IonicErrorHandler }
  ]
})
export class AppModule { }
