import { ErrorHandlerCustom } from './error-handler';
import { API_URL } from './app.api';
import { Injectable } from '@angular/core';
import { RequestOptions, URLSearchParams, Headers } from '@angular/http';
import { HttpClient, HttpParams } from '@angular/common/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { Jsonp } from '@angular/http';
import { DatePipe } from '@angular/common';
import 'rxjs/add/operator/map';

@Injectable()

export class Interceptor {

    constructor(private _http: HttpClient, private _Jsonp: Jsonp, private datePipe: DatePipe) { }


    post(url, bodyRequest) {

        //let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
        //let options = new RequestOptions({ headers });
        //let body = new HttpParams(bodyRequest);
        //console.warn(bodyRequest.toString());
        return this._http.post(API_URL + url, bodyRequest).map(Response => Response).catch(ErrorHandlerCustom.HandlerError);

    }

    get(url, bodyRequest) {

        //let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
        //let options = new RequestOptions({ headers });
    
        return this._http.get(API_URL + url).map(Response => Response).catch(ErrorHandlerCustom.HandlerError);

    }

    put(url, bodyRequest) {

        //let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
        //let options = new RequestOptions({ headers });
        //let body = new HttpParams(bodyRequest);

        return this._http.put(API_URL + url, bodyRequest).map(Response => Response).catch(ErrorHandlerCustom.HandlerError);
    }

    delete(url){
        
        //let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
        //let options = new RequestOptions({ headers });

        return this._http.delete(API_URL + url).map(Response => Response).catch(ErrorHandlerCustom.HandlerError);
    }

}