import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ColorsambientePage } from './colorsambiente';

@NgModule({
  declarations: [
    ColorsambientePage,
  ],
  imports: [
    IonicPageModule.forChild(ColorsambientePage),
  ],
})
export class ColorsambientePageModule {}
