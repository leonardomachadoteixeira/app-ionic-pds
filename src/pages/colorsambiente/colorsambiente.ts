import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { DBMeter } from '@ionic-native/db-meter';

/**
 * Generated class for the ColorsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-colorsambiente',
  templateUrl: 'colorsambiente.html',
})
export class ColorsambientePage {

  subscription: any;
  cur = 'black';
  currence = 'black';
  refreshIntervalId: any;
  intervalAverage: any;
  low: number = 10;
  medium: number = 10;
  high: number = 10;
  veryHigh: number = 10;
  vetorSonds = [] = [10];
  average: number = 10;
  bolCalib: boolean = true;

  constructor(public navCtrl: NavController, public navParams: NavParams, private dbMeter: DBMeter) {

  }

  ionViewDidLoad() {
    setTimeout(() => {
      this.bolCalib = false;
    }, 4000)
    console.log('ionViewDidLoad ColorsPage');
    this.refreshIntervalId = setInterval(() => {
      if (this.cur != this.currence) {
        document.querySelector('#boxcolor').classList.remove(this.currence);
        document.querySelector('#boxcolor').classList.add(this.cur);
        this.currence = this.cur;
      }
    }, 500);


    this.intervalAverage = setInterval(() => {
      this.vetorSonds.forEach(element => {
        this.average = this.average + element;
      });
      this.average = this.average / this.vetorSonds.length;
      //console.warn(this.average);
      this.low = this.average - 10 | 0;
      this.medium = this.average + 10 | 0;
      this.high = this.average + 25 | 0;
      // console.warn(this.low);
      this.average = 0;
      this.vetorSonds.splice(0, 1);

    }, 5000)


    this.subscription = this.dbMeter.start().subscribe(
      data => {
        if (this.vetorSonds.length <= 5) {
          this.vetorSonds.push(data);
        }
        let color = '';
        if (data > 0 && data <= this.low) {
          color = 'black';
        } else if (data > this.low && data <= this.medium) {
          color = 'blue';
        } else if (data > this.medium && data <= this.high) {
          color = 'orange';
        } else if (data > this.high) {
          color = 'red';
        }

        if (this.cur != color) {
          this.cur = color;
        }
      }
    )
  }

  ionViewWillLeave() {
    console.log("Looks like I'm about to leave :(");
    this.subscription.unsubscribe();
    clearInterval(this.refreshIntervalId);
    clearInterval(this.intervalAverage);
  }
}
