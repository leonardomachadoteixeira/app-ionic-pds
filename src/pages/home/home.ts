import { Interceptor } from './../../auth/interceptor';
import { ColorsambientePage } from './../colorsambiente/colorsambiente';
import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Network } from '@ionic-native/network';
import { Dialogs } from '@ionic-native/dialogs';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  subscription;
  formData = {
    name: ''
  }

  constructor(private _interceptor: Interceptor, private dialogs: Dialogs, private network: Network, public navCtrl: NavController) {

  }

  PushPage() {
    
    let type = this.network.type

    if (type == 'none' || type == 'unknown') {
      this.dialogs.alert('Para usar esta função, se conecte a internet!', 'Ops...')
        .then(() => console.log('Dialog dismissed'))
        .catch(e => console.log('Error displaying dialog', e));
    } else {
      let data = {
        'name': this.formData.name
      }
      this._interceptor.post('rooms/search', data)
        .subscribe(Response => {
          this.dialogs.alert('Sala não encontrada!', 'Ops...')
            .then(() => console.log('Dialog dismissed'))
            .catch(e => console.log('Error displaying dialog', e));

        }, (error) => {
            this.navCtrl.push("ColorsPage", {room: this.formData.name});
        });
    }
  }

  Pushambiente() {
    this.navCtrl.push(ColorsambientePage);
  }

}
